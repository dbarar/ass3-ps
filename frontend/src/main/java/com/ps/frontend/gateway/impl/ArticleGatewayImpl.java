package com.ps.frontend.gateway.impl;

import com.ps.common.dto.ArticleDTO;
import com.ps.frontend.gateway.ArticleGateway;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;
import java.util.List;

@Component
public class ArticleGatewayImpl implements ArticleGateway{
    private static final Logger LOGGER = LoggerFactory.getLogger(UserGatewayImpl.class);
    private final String URL = "/article";

    private final RestProperties restProperties;

    @Autowired
    public ArticleGatewayImpl(RestProperties restProperties) {
        this.restProperties = restProperties;
    }

    @Override
    public String testArticle() {
        LOGGER.info("Testing test() method, connection to backend api from article");
        String url = restProperties.getUrl() + URL + "/test";
        RestTemplate restTemplate = new RestTemplate();
        ResponseEntity<String> forEntity = restTemplate.getForEntity(url, String.class);
        return restTemplate.getForObject(url, String.class);
    }

    @Override
    public List<ArticleDTO> findAll() {
        LOGGER.info("Executing find all on articles");
        String url = restProperties.getUrl() + URL + "/list";
        RestTemplate restTemplate = new RestTemplate();
        ResponseEntity<ArticleDTO[]> forEntity = restTemplate.getForEntity(url, ArticleDTO[].class);
        ArticleDTO[] response = restTemplate.getForObject(url, ArticleDTO[].class);
        return Arrays.asList(response);
    }

    @Override
    public ArticleDTO findById(Long id) {
        LOGGER.info("Executing findById method, id=" + id);
        String url = restProperties.getUrl() + URL + "/" + id;
        RestTemplate restTemplate = new RestTemplate();
        ResponseEntity<ArticleDTO> forEntity = restTemplate.getForEntity(url, ArticleDTO.class);
        ArticleDTO articleDTO = restTemplate.getForObject(url, ArticleDTO.class);
        return articleDTO;
    }
}
