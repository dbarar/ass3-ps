package com.ps.frontend.gateway;

import com.ps.common.dto.ArticleDTO;

import java.util.List;

public interface WriterGateway {

    Long saveArticle(ArticleDTO articleDTO);

    Long updateArticle(ArticleDTO articleDTO);

    Boolean deleteArticle(Long id);

    List<ArticleDTO> listArticles();
}
