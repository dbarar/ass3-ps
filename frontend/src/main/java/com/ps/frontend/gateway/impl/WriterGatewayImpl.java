package com.ps.frontend.gateway.impl;

import com.ps.common.dto.ArticleDTO;
import com.ps.frontend.gateway.WriterGateway;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;
import java.util.List;

@Component
public class WriterGatewayImpl implements WriterGateway{

    private static final Logger LOGGER = LoggerFactory.getLogger(UserGatewayImpl.class);
    private final String URL = "/writer";

    private final RestProperties restProperties;

    @Autowired
    public WriterGatewayImpl(RestProperties restProperties) {
        this.restProperties = restProperties;
    }

    @Override
    public Long saveArticle(ArticleDTO articleDTO) {
        LOGGER.info("Executing save method");
        String url = restProperties.getUrl() + URL + "/saveArticle";
        HttpEntity<Object> httpEntity = new HttpEntity<>(articleDTO);
        RestTemplate restTemplate = new RestTemplate();
        Long response = restTemplate.postForObject(url, httpEntity, Long.class);
        return response;
    }

    @Override
    public Long updateArticle(ArticleDTO articleDTO) {
        LOGGER.info("Executing update method");
        String url = restProperties.getUrl() + URL + "/updateArticle";
        HttpEntity<Object> httpEntity = new HttpEntity<>(articleDTO);
        RestTemplate restTemplate = new RestTemplate();
        Long response = restTemplate.postForObject(url, httpEntity, Long.class);
        return response;
    }

    @Override
    public Boolean deleteArticle(Long id) {
        LOGGER.info("Executing delete article method");
        String url = restProperties.getUrl() + URL + "/" + id + "/deleteArticle";
        RestTemplate restTemplate = new RestTemplate();
        ResponseEntity<Boolean> forEntity = restTemplate.getForEntity(url, Boolean.class);
        Boolean bool = restTemplate.getForObject(url, Boolean.class);
        return bool;
    }

    @Override
    public List<ArticleDTO> listArticles() {
        LOGGER.info("Executing listArticles method");
        String url = restProperties.getUrl() + URL + "/listArticles";
        RestTemplate restTemplate = new RestTemplate();
        ResponseEntity<ArticleDTO[]> forEntity = restTemplate.getForEntity(url, ArticleDTO[].class);
        ArticleDTO[] response = restTemplate.getForObject(url, ArticleDTO[].class);
        return Arrays.asList(response);
    }
}
