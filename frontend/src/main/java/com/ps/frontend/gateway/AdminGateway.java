package com.ps.frontend.gateway;

import com.ps.common.dto.CategoryDTO;

import java.util.List;

public interface AdminGateway {

    Long saveCategory(CategoryDTO categoryDTO);

    Boolean deleteCategory(Long id);

    List<CategoryDTO> listCategories();
}
