package com.ps.frontend.gateway;

import com.ps.common.dto.ArticleDTO;

import java.util.List;

public interface ArticleGateway {

    String testArticle();

    List<ArticleDTO> findAll();

    ArticleDTO findById(Long id);
}
