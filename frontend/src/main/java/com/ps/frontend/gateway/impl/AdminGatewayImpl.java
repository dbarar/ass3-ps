package com.ps.frontend.gateway.impl;

import com.ps.common.dto.CategoryDTO;
import com.ps.frontend.gateway.AdminGateway;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;
import java.util.List;

@Component
public class AdminGatewayImpl implements AdminGateway {
    private static final Logger LOGGER = LoggerFactory.getLogger(UserGatewayImpl.class);
    private final String URL = "/admin";

    private final RestProperties restProperties;

    @Autowired
    public AdminGatewayImpl(RestProperties restProperties) {
        this.restProperties = restProperties;
    }

    @Override
    public Long saveCategory(CategoryDTO categoryDTO) {
        LOGGER.info("Executing save method");
        String url = restProperties.getUrl() + URL + "/saveCategory";
        HttpEntity<Object> httpEntity = new HttpEntity<>(categoryDTO);
        RestTemplate restTemplate = new RestTemplate();
        Long response = restTemplate.postForObject(url, httpEntity, Long.class);
        return response;
    }

    @Override
    public Boolean deleteCategory(Long id) {
        LOGGER.info("Executing delete category method");
        String url = restProperties.getUrl() + URL + "/" + id + "/deleteCategory";
        RestTemplate restTemplate = new RestTemplate();
        ResponseEntity<Boolean> forEntity = restTemplate.getForEntity(url, Boolean.class);
        Boolean bool = restTemplate.getForObject(url, Boolean.class);
        return bool;
    }

    @Override
    public List<CategoryDTO> listCategories() {
        LOGGER.info("Executing listArticles method");
        String url = restProperties.getUrl() + URL + "/listCategories";
        RestTemplate restTemplate = new RestTemplate();
        ResponseEntity<CategoryDTO[]> forEntity = restTemplate.getForEntity(url, CategoryDTO[].class);
        CategoryDTO[] response = restTemplate.getForObject(url, CategoryDTO[].class);
        return Arrays.asList(response);
    }
}
