package com.ps.frontend.controller;

import com.ps.common.dto.ArticleDTO;
import com.ps.frontend.gateway.WriterGateway;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Conditional;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.validation.constraints.AssertTrue;
import java.util.List;

@Controller
@RequestMapping("/writer")
public class WriterController {
    private final WriterGateway writerGateway;
    private final UserController userController;

    @Autowired
    public WriterController(WriterGateway writerGateway, UserController userController) {
        this.writerGateway = writerGateway;
        this.userController = userController;
    }

    @GetMapping("/listArticles")
    public ModelAndView list(ModelAndView mav){
        if(userController.getLoggedIn()){

            List<ArticleDTO> all = writerGateway.listArticles();

            mav.addObject("articles", all);
            mav.setViewName("article/list");
            return mav;
        }
        return mav;
    }

    @GetMapping("/createArticle")
    public String openCreate(){
        if(userController.getLoggedIn()) {
            return "article/create";
        }
        return "redirect:/user/login";
    }

    @PostMapping("/createArticle")
    public String createArticle(ArticleDTO articleDTO){
        writerGateway.saveArticle(articleDTO);
        return "redirect:/writer/listArticles";
    }

    @GetMapping("/updateArticle")
    public String openUpdate(){
        if(userController.getLoggedIn()) {
            return "article/create";
        }
        return "redirect:/user/login";
    }

    @PostMapping("/updateArticle")
    public String updateArticle(ArticleDTO articleDTO){
        writerGateway.updateArticle(articleDTO);
        return "redirect:/writer/listArticles";
    }

    @GetMapping("/{id}/deleteArticle")
    public String deleteArticle(@PathVariable("id") Long id) {
        if (userController.getLoggedIn()) {
            writerGateway.deleteArticle(id);
            return "redirect:/writer/listArticles";
        }
        return "redirect:/user/login";
    }
}
