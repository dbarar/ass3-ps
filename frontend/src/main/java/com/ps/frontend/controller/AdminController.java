package com.ps.frontend.controller;

import com.ps.common.dto.CategoryDTO;
import com.ps.common.dto.UserDTO;
import com.ps.common.enumeration.ArticleCategory;
import com.ps.common.enumeration.UserRole;
import com.ps.frontend.gateway.AdminGateway;
import com.ps.frontend.gateway.UserGateway;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

@Controller
@RequestMapping("/admin")
public class AdminController {
    private final AdminGateway adminGateway;
    private final UserGateway userGateway;
    private final UserController userController;

    @Autowired
    public AdminController(AdminGateway adminGateway, UserGateway userGateway, UserController userController) {
        this.adminGateway = adminGateway;
        this.userGateway = userGateway;
        this.userController = userController;
    }

    @GetMapping("/createUser")
    public String openCreateUser() {
        if(userController.getLoggedIn()) {
            return "user/create";
        }
        return "redirect:/user/login";
    }

    @PostMapping("/createUser")
    public String create(UserDTO userDTO) {
        if(userDTO.getRole() == UserRole.WRITER)
            userGateway.save(userDTO);
        return "redirect:/user/list";
    }

    @GetMapping("/listCategories")
    public ModelAndView list(ModelAndView mav) {
        if(userController.getLoggedIn()) {
            List<CategoryDTO> all = adminGateway.listCategories();

            mav.addObject("categories", all);
            mav.setViewName("category/list");
            return mav;
            }
        return mav;
    }

    @GetMapping("/createCategory")
    public String openCreateCategory(){
        if(userController.getLoggedIn()) {
            return "category/create";
        }
        return "redirect:/user/login";
    }

    @PostMapping("/createCategory")
    public String createArticle(CategoryDTO categoryDTO){
        CategoryDTO c = new CategoryDTO();
        adminGateway.saveCategory(categoryDTO);
        return "redirect:/admin/listCategories";
    }

    @GetMapping("/{id}/deleteCategory")
    public String deleteArticle(@PathVariable("id") Long id) {
        if (userController.getLoggedIn()) {
            adminGateway.deleteCategory(id);
            return "redirect:/admin/listCategories";
        }
        return "redirect:/user/login";
    }
}
