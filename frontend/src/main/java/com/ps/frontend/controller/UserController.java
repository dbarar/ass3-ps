package com.ps.frontend.controller;

import com.ps.common.dto.LoginDTO;
import com.ps.common.dto.UserDTO;
import com.ps.common.enumeration.UserRole;
import com.ps.frontend.gateway.UserGateway;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

@Controller
@RequestMapping("/user")
public class UserController {

    private final UserGateway userGateway;
    private Boolean loggedIn = false;

    @Autowired
    public UserController(UserGateway userGateway) {
        this.userGateway = userGateway;
    }

    @GetMapping("/test")
    @ResponseStatus(HttpStatus.OK)
    public void getTest() {
        String test = userGateway.test();
        System.out.println(test);
    }

    @GetMapping("/{id}")
    public ModelAndView details(@PathVariable("id") Long id, ModelAndView mav) {
        UserDTO user = userGateway.findById(id);

        mav.addObject("user", user);
        mav.setViewName("user/details");
        return mav;
    }

    @GetMapping("/list")
    public ModelAndView list(ModelAndView mav) {
        List<UserDTO> all = userGateway.findAll();

        mav.addObject("users", all);
        mav.setViewName("user/list");
        return mav;
    }

    @GetMapping("/create")
    public String openCreate() {
        return "user/create";
    }

    @PostMapping("/create")
    public String create(UserDTO userDTO) {
        userGateway.save(userDTO);
        return "redirect:/user/list";
    }

    @GetMapping("/login")
    public String openLogin(){ return "user/login";}

    @PostMapping("/login")
    public String login(LoginDTO loginDTO){
        UserDTO userDTO = userGateway.login(loginDTO);
        if (userDTO.getRole() == UserRole.WRITER){
            loggedIn = Boolean.TRUE;
            return "redirect:/writer/listArticles";
        }
        else if(userDTO.getRole() == UserRole.ADMIN){
            loggedIn = Boolean.TRUE;
            return  "redirect:/admin/listCategories";
        }
        else
            return "redirect:/login";
    }

    public Boolean getLoggedIn() {
        return loggedIn;
    }
}

