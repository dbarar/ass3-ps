package com.ps.frontend.controller;

import com.ps.common.dto.ArticleDTO;
import com.ps.frontend.gateway.ArticleGateway;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

@Controller
@RequestMapping("/article")
public class ArticleController {

    private final ArticleGateway articleGateway;

    @Autowired
    public ArticleController(ArticleGateway articleGateway) {
        this.articleGateway = articleGateway;
    }

    @GetMapping("/test")
    @ResponseStatus(HttpStatus.OK)
    public void getTest(){
        String test = articleGateway.testArticle();
        System.out.println(test);
    }

    @GetMapping("/list")
    public ModelAndView list(ModelAndView mav){
        List<ArticleDTO> all = articleGateway.findAll();

        mav.addObject("articles", all);
        mav.setViewName("article/list");
        return mav;
    }

    @GetMapping("/{id}")
    public ModelAndView details(@PathVariable("id") Long id, ModelAndView mav) {
        ArticleDTO articleDTO = articleGateway.findById(id);

        mav.addObject("article", articleDTO);
        mav.setViewName("article/details");
        return mav;
    }
}
