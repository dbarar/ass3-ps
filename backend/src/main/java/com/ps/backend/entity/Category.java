package com.ps.backend.entity;

import com.ps.common.enumeration.ArticleCategory;

import javax.persistence.*;
import java.util.List;

@Entity
public class Category {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Enumerated(value = EnumType.STRING)
    private ArticleCategory categoryName;

    @OneToMany(mappedBy = "categoryId")
    private List<Article> articlesSameCategory;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public ArticleCategory getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(ArticleCategory categoryName) {
        this.categoryName = categoryName;
    }

    public List<Article> getArticlesSameCategory() {
        return articlesSameCategory;
    }

    public void setArticlesSameCategory(List<Article> articlesSameCategory) {
        this.articlesSameCategory = articlesSameCategory;
    }
}
