package com.ps.backend.rest;

import com.ps.common.dto.CategoryDTO;
import com.ps.common.dto.UserDTO;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RequestMapping("/admin")
public interface AdminRestApi {
    @PostMapping("/saveUser")
    Long saveUser(@RequestBody UserDTO userDTO);

    @PostMapping("/saveCategory")
    Long saveCategory(@RequestBody CategoryDTO categoryDTO);

    @GetMapping("/{id}/deleteCategory")
    Boolean deleteCategory(@PathVariable("id") Long id);

    @GetMapping("/listCategories")
    List<CategoryDTO> listCategories();
}
