package com.ps.backend.rest;

import com.ps.common.dto.ArticleDTO;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RequestMapping("/article")
public interface ArticleRestApi {

    @GetMapping("/test")
    String testArticle();

    @GetMapping("/list")
    List<ArticleDTO> findAll();

    @GetMapping("{id}")
    ArticleDTO findById(@PathVariable("id") Long id);
}
