package com.ps.backend.rest;

import com.ps.backend.service.ArticleService;
import com.ps.common.dto.ArticleDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class ArticleRestController implements ArticleRestApi {

    private final ArticleService articleService;

    @Autowired
    public ArticleRestController(ArticleService articleService){
        this.articleService = articleService;
    }

    @Override
    public String testArticle() {
        return articleService.testArticle();
    }

    @Override
    public List<ArticleDTO> findAll() {
        return articleService.findAll();
    }

    @Override
    public ArticleDTO findById(@PathVariable("id") Long id) {
        return articleService.findById(id);
    }
}
