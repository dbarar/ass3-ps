package com.ps.backend.rest;

import com.ps.common.dto.ArticleDTO;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RequestMapping("/writer")
public interface WriterRestApi {

    @PostMapping("/saveArticle")
    Long saveArticle(@RequestBody ArticleDTO articleDTO);

    @PostMapping("/updateArticle")
    Long updateArticle(@RequestBody ArticleDTO articleDTO);

    @GetMapping("/{id}/deleteArticle")
    Boolean deleteArticle(@PathVariable("id") Long id);

    @GetMapping("/listArticles")
    List<ArticleDTO> listArticles();
}
