package com.ps.backend.rest;

import com.ps.backend.service.ArticleService;
import com.ps.backend.service.WriterService;
import com.ps.common.dto.ArticleDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class WriterRestController implements WriterRestApi {

    private final WriterService writerService;
    private final ArticleService articleService;

    @Autowired
    public WriterRestController(WriterService writerService, ArticleService articleService) {
        this.writerService = writerService;
        this.articleService = articleService;
    }

    @Override
    public Long saveArticle(@RequestBody ArticleDTO articleDTO) {
        return writerService.saveArticle(articleDTO);
    }

    @Override
    public Long updateArticle(@RequestBody ArticleDTO articleDTO) {
        return writerService.updateArticle(articleDTO);
    }

    @Override
    public Boolean deleteArticle(@PathVariable("id") Long id) {
        return writerService.deleteArticle(id);
    }

    @Override
    public List<ArticleDTO> listArticles() {
        return articleService.findAll();
    }
}
