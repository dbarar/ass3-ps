package com.ps.backend.rest;

import com.ps.backend.service.AdminService;
import com.ps.backend.service.UserService;
import com.ps.common.dto.CategoryDTO;
import com.ps.common.dto.UserDTO;
import com.ps.common.enumeration.UserRole;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class AdminRestController  implements AdminRestApi{
    private final AdminService adminService;
    private final UserService userService;

    @Autowired
    public AdminRestController(AdminService adminService, UserService userService) {
        this.adminService = adminService;
        this.userService = userService;
    }

    @Override
    public Long saveUser(@RequestBody UserDTO userDTO) {
        if(userDTO.getRole() == UserRole.WRITER)
            return userService.save(userDTO);
        else
            return -1L;
    }

    @Override
    public Long saveCategory(@RequestBody CategoryDTO categoryDTO) {
        return adminService.saveCategory(categoryDTO);
    }

    @Override
    public Boolean deleteCategory(@PathVariable("id") Long id) {
        return adminService.deleteCategory(id);
    }

    @Override
    public List<CategoryDTO> listCategories() {
        return adminService.listCategories();
    }
}
