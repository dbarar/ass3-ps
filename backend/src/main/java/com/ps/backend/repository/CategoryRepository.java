package com.ps.backend.repository;

import com.ps.backend.entity.Category;
import com.ps.common.enumeration.ArticleCategory;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface CategoryRepository extends JpaRepository<Category, Long> {
    @Query("SELECT c FROM Category c WHERE (c.categoryName = :categoryName)")
    Category findByCategoryName(@Param("categoryName") ArticleCategory categoryName);
}
