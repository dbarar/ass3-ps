package com.ps.backend.service.impl;

import com.ps.backend.entity.Category;
import com.ps.backend.repository.CategoryRepository;
import com.ps.backend.repository.UserRepository;
import com.ps.backend.service.AdminService;
import com.ps.common.dto.CategoryDTO;
import com.ps.common.dto.UserDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class AdminServiceImpl implements AdminService{

    private final CategoryRepository categoryRepository;
    private final UserRepository userRepository;

    @Autowired
    public AdminServiceImpl(CategoryRepository categoryRepository, UserRepository userRepository) {
        this.categoryRepository = categoryRepository;
        this.userRepository = userRepository;
    }

    @Override
    public Long saveCategory(CategoryDTO categoryDTO) {
        Optional<Category> optionalCategory;

        if(categoryDTO.getId() == null) {
            optionalCategory = Optional.empty();
        } else {
            optionalCategory = categoryRepository.findById(categoryDTO.getId());
        }

        Category category = optionalCategory.isPresent() ? optionalCategory.get() : new Category();
        category.setCategoryName(categoryDTO.getCategoryName());

        return categoryRepository.save(category).getId();
    }

    @Override
    public Boolean deleteCategory(Long id) {
        Optional<Category> optionalArticle;

        if(id == null){
            optionalArticle = Optional.empty();
        }else{
            optionalArticle = categoryRepository.findById(id);
        }
        Category category = optionalArticle.isPresent() ? optionalArticle.get() : new Category();

        categoryRepository.delete(category);
        return true;
    }

    @Override
    public List<CategoryDTO> listCategories() {
        return categoryRepository.findAll().stream().map(entity -> {
            CategoryDTO categoryDTO = new CategoryDTO();
            categoryDTO.setId(entity.getId());
            categoryDTO.setCategoryName(entity.getCategoryName());
            return categoryDTO;
        }).collect(Collectors.toList());
    }
}
