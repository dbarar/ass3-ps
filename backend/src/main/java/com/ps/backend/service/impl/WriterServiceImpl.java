package com.ps.backend.service.impl;

import com.ps.backend.entity.Article;
import com.ps.backend.entity.Category;
import com.ps.backend.repository.ArticleRepository;
import com.ps.backend.repository.CategoryRepository;
import com.ps.backend.repository.UserRepository;
import com.ps.backend.service.WriterService;
import com.ps.common.dto.ArticleDTO;
import com.ps.common.enumeration.ArticleCategory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import javax.transaction.Transactional;
import java.util.Optional;

@Service
public class WriterServiceImpl implements WriterService{
    private final ArticleRepository articleRepository;
    private final CategoryRepository categoryRepository;

    @Autowired
    public WriterServiceImpl(ArticleRepository articleRepository, CategoryRepository categoryRepository) {
        this.articleRepository = articleRepository;
        this.categoryRepository = categoryRepository;
    }

    @Override
    @Transactional
    public Long saveArticle(ArticleDTO articleDTO) {
        Optional<Article> optionalArticle;

        if(articleDTO.getId() == null){
            optionalArticle = Optional.empty();
        }else{
            optionalArticle = articleRepository.findById(articleDTO.getId());
        }

        Article article = optionalArticle.isPresent() ? optionalArticle.get() : new Article();
        article.setAbstractArticle(articleDTO.getAbstractArticle());
        article.setAuthor(articleDTO.getAuthor());
        article.setBody(articleDTO.getBody());
        article.setCategoryId(findCategoryId(articleDTO.getArticleCategory()));
        article.setTitle(articleDTO.getTitle());

        return articleRepository.save(article).getId();
    }

    private Long findCategoryId(ArticleCategory articleCategory){
        Category category = categoryRepository.findByCategoryName(articleCategory);
        if(category == null)
            throw new EntityNotFoundException("Cannot find category");
        return category.getId();
    }

    @Override
    @Transactional
    public Long updateArticle(ArticleDTO articleDTO) {
        return saveArticle(articleDTO);
    }

    @Override
    @Transactional
    public Boolean deleteArticle(Long id) {
        Optional<Article> optionalArticle;

        if(id == null){
            optionalArticle = Optional.empty();
        }else{
            optionalArticle = articleRepository.findById(id);
        }
        Article article = optionalArticle.isPresent() ? optionalArticle.get() : new Article();

        articleRepository.delete(article);
        return true;
    }
}
