package com.ps.backend.service;

import com.ps.common.dto.ArticleDTO;

import java.util.List;

public interface ArticleService {

    String testArticle();

    ArticleDTO findById(Long id);

    List<ArticleDTO> findAll();
}
