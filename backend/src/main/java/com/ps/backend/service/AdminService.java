package com.ps.backend.service;

import com.ps.common.dto.CategoryDTO;
import com.ps.common.dto.UserDTO;

import java.util.List;

public interface AdminService {

    Long saveCategory(CategoryDTO categoryDTO);

    Boolean deleteCategory(Long id);

    List<CategoryDTO> listCategories();
}
