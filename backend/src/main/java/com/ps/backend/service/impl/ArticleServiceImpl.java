package com.ps.backend.service.impl;

import com.ps.backend.entity.Article;
import com.ps.backend.entity.Category;
import com.ps.backend.repository.ArticleRepository;
import com.ps.backend.repository.CategoryRepository;
import com.ps.backend.service.ArticleService;
import com.ps.common.dto.ArticleDTO;
import com.ps.common.dto.ArticleDescriptionDTO;
import com.ps.common.enumeration.ArticleCategory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
public class ArticleServiceImpl implements ArticleService {
    private final ArticleRepository articleRepository;
    private final CategoryRepository categoryRepository;

    @Autowired
    public ArticleServiceImpl(ArticleRepository articleRepository, CategoryRepository categoryRepository) {
        this.articleRepository = articleRepository;
        this.categoryRepository = categoryRepository;
    }

    @Override
    public String testArticle() {
        return "service article working";
    }

    @Override
    public List<ArticleDTO> findAll() {
        return articleRepository.findAll()
                .stream()
                .map(entity ->{
                    ArticleDTO articleDTO = new ArticleDTO();
                    articleDTO.setId(entity.getId());
                    articleDTO.setAbstractArticle(entity.getAbstractArticle());
                    articleDTO.setAuthor(entity.getAuthor());
                    articleDTO.setBody(entity.getBody());
                    articleDTO.setArticleCategory(findArticleCategory(entity.getCategoryId()));
                    articleDTO.setTitle(entity.getTitle());
                    articleDTO.setRelatedArticles(getArticleDescriptionDTOStream(entity).collect(Collectors.toList()));
                    return articleDTO;
                }).collect(Collectors.toList());
    }

    @Override
    public ArticleDTO findById(Long id) {
        Article entity = articleRepository.findById(id)
                .orElseThrow(() -> new EntityNotFoundException("Cannot find article with id " + id));
        ArticleDTO articleDTO = new ArticleDTO();
        articleDTO.setId(entity.getId());
        articleDTO.setAbstractArticle(entity.getAbstractArticle());
        articleDTO.setAuthor(entity.getAuthor());
        articleDTO.setBody(entity.getBody());
        articleDTO.setArticleCategory(findArticleCategory(entity.getCategoryId()));
        articleDTO.setTitle(entity.getTitle());
        articleDTO.setRelatedArticles(getArticleDescriptionDTOStream(entity).collect(Collectors.toList()));
        return articleDTO;
    }

    private ArticleCategory findArticleCategory(Long id){
        Category category = categoryRepository.findById(id).
                orElseThrow(() -> new EntityNotFoundException("Cannot find category with id " + id));
        return category.getCategoryName();
    }

    private Stream<ArticleDescriptionDTO> getArticleDescriptionDTOStream(Article entity) {
        Long categoryID = entity.getCategoryId();
        Category category = categoryRepository.findById(categoryID).
                orElseThrow(() -> new EntityNotFoundException("Cannot find article with id " + categoryID));
        return category.getArticlesSameCategory()
                .stream()
                .map(e -> {
                            ArticleDescriptionDTO articleDescriptionDTO = new ArticleDescriptionDTO();
                            articleDescriptionDTO.setId(e.getId());
                            articleDescriptionDTO.setTitle(e.getTitle());
                            articleDescriptionDTO.setAbstractArticle(e.getAbstractArticle());
                            return articleDescriptionDTO;
                        }
                );
    }
}
