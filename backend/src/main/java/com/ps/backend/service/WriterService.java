package com.ps.backend.service;

import com.ps.common.dto.ArticleDTO;

public interface WriterService {

    Long saveArticle(ArticleDTO articleDTO);

    Long updateArticle(ArticleDTO articleDTO);

    Boolean deleteArticle(Long id);
}
