package com.ps.common.enumeration;

public enum ArticleCategory {
    SCIENCE,
    ART,
    COMPUTER,
    CAR,
    EDUCATION,
    SF,
    MATH
}
