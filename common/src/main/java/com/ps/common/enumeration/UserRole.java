package com.ps.common.enumeration;

public enum UserRole {
    WRITER,
    ADMIN
}
