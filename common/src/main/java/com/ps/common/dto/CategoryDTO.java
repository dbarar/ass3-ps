package com.ps.common.dto;

import com.ps.common.enumeration.ArticleCategory;

public class CategoryDTO {

    private Long id;

    private ArticleCategory categoryName;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public ArticleCategory getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(ArticleCategory categoryName) {
        this.categoryName = categoryName;
    }
}
