package com.ps.common.dto;

import com.ps.common.enumeration.ArticleCategory;

import java.util.List;

public class ArticleDTO {
    private Long id;
    private String title;
    private String abstractArticle;
    private String author;
    private ArticleCategory articleCategory;
    private String body;
    private List<ArticleDescriptionDTO> relatedArticles;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAbstractArticle() {
        return abstractArticle;
    }

    public void setAbstractArticle(String abstractArticle) {
        this.abstractArticle = abstractArticle;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public ArticleCategory getArticleCategory() {
        return articleCategory;
    }

    public void setArticleCategory(ArticleCategory articleCategory) {
        this.articleCategory = articleCategory;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public List<ArticleDescriptionDTO> getRelatedArticles() {
        return relatedArticles;
    }

    public void setRelatedArticles(List<ArticleDescriptionDTO> relatedArticles) {
        this.relatedArticles = relatedArticles;
    }
}
